package com.example.cse118_group7;

public class BacCalc {
	private static final double MALE_CONST = 0.58;
	private static final double FEMALE_CONST = 0.49;
	private static final double MALE_MET = 0.015;
	private static final double FEMALE_MET = 0.017;
	
	/**
	 * 
	 * @param gender male or female
	 * @param weight Weight in pounds
	 * @param numDrinks
	 * @param time Elapsed time in miliseconds
	 * @return
	 */
	public static double calculate(String gender, double weight, int numDrinks, double time)
	{
		double bac;
		double elapsed = time / 3600000; // Time elapsed in hours
		
		if (gender.toLowerCase().equals("male"))
		{
			bac = (0.806 * numDrinks * 1.2) / (MALE_CONST * weight * 0.454) - (MALE_MET * elapsed);
		}
		else
		{
			bac = (0.806 * numDrinks * 1.2) / (FEMALE_CONST * weight * 0.454) - (FEMALE_MET * elapsed);
		}
		
		return bac;
	}
	
	/**
	 * 
	 * @param gender male or female
	 * @param weight Weight in pounds
	 * @param numDrinks
	 * @param bac Current bac level
	 * @return time in minutes
	 */
	public static double calculateTime(double bac)
	{
		double bacTarget = 0.07;
		double hours;
		
		hours = (bac - bacTarget) / 0.015;
		
		return hours;
	}

}
