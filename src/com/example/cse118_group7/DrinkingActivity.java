package com.example.cse118_group7;

import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MotionEvent;
import android.widget.TextView;
import android.widget.Toast;

public class DrinkingActivity extends Activity  implements SensorEventListener, GestureDetector.OnGestureListener {
	private static final String WEIGHT_ = "weight";
	private static final String GENDER_ = "gender";
	private static final double THRESHHOLD = 0.84;
	private TextView bac, status, drinksHad, waitTime;

	private String mGender;
	private int mWeight;
	private double yAxisAccel;
	private long startTime;
	private long timeLastDrink;
	private int numDrinks;
	SensorManager mSensorManager;
	private Handler mHandler;
	private int numTouches =0;
	private GestureDetector gd;
	private int wait = 0;
	
	Runnable thread = new Runnable() {
		@Override
		public void run() {
			updateFields();
			mHandler.postDelayed(thread,300000);
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_drinking);
		Intent i = getIntent();
		Bundle b = i.getExtras();
		mGender = b.getString(GENDER_);
		mWeight = b.getInt(WEIGHT_);
		bac = (TextView) findViewById(R.id.bac_field);
		status = (TextView) findViewById(R.id.status_field);
		drinksHad = (TextView) findViewById(R.id.num_drinks_field);
		waitTime = (TextView) findViewById(R.id.wait_time_field);
		bac.setText(""+0.0);
		status.setText("OK to drive");
		drinksHad.setText(""+0);
		waitTime.setText("0 hours and 0 minutes");
		yAxisAccel = 0;
		startTime = 0;
		numDrinks = 0;
		timeLastDrink = 0;
		mHandler = new Handler();
		
		mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		
		mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY), SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR), SensorManager.SENSOR_DELAY_NORMAL);
        
        gd = new GestureDetector(this, this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.drinking, menu);
		return true;
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
	   if(event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR)
	   {
		   if(event.values[0] > THRESHHOLD)
		   {
			   if(numDrinks == 0)
			   {
				   startTime = System.currentTimeMillis();
				   numDrinks++;
				   updateFields();
				   timeLastDrink = System.currentTimeMillis();
				   thread.run();
			   }
			   else if (System.currentTimeMillis() - timeLastDrink > 2000) {
				   numDrinks++;
				   updateFields();
				   timeLastDrink = System.currentTimeMillis();
			   }
			   
		   }
		//.65 eye level .84 
	   }

	}
	
	public void updateFields() {
		long currentTime = System.currentTimeMillis() - startTime;
		double bacValue = BacCalc.calculate(mGender, mWeight, numDrinks, currentTime);
		
		drinksHad.setText(""+numDrinks);
		bac.setText(""+bacValue);
		if(bacValue < .05) {
			status.setText(R.string.can_drive_txt);
		} else if( bacValue < .08){
			status.setText(R.string.maybe_drive_txt);
		} else {
			status.setText(R.string.cant_drive_txt);
		}
		double time = BacCalc.calculateTime(BacCalc.calculate(mGender, mWeight, numDrinks, System.currentTimeMillis() - startTime));
		
		if (time < 0) {
			waitTime.setText(""+ 0 + " hours and " + 0 + " minutes");
		}
		else {
			int hours = (int) time;
			int minutes = (int) (time *60) % 60;
			waitTime.setText(""+ hours + " hours and " + minutes + " minutes");
		}
		
	}

	@Override
	public boolean onDown(MotionEvent arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onFling(MotionEvent arg0, MotionEvent arg1, float arg2,
			float arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onLongPress(MotionEvent arg0) {
		bac.setText(""+0.0);
		status.setText("OK");
		drinksHad.setText(""+0);
		yAxisAccel = 0;
		startTime = 0;
		numDrinks = 0;
	}
	
	@Override
	public void onDestroy() {
		mHandler.removeCallbacks(thread);
	}

	@Override
	public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2,
			float arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onShowPress(MotionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent arg0) {
		long curr =System.currentTimeMillis() - timeLastDrink;
		int min = (int)(curr/1000) / 60;
		long sec = (curr/1000) % 60;
		
		if(timeLastDrink == 0) {
			Toast.makeText(this, "Time since last drink: 0 minutes and 0 seconds ago", Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(this, "Time since last drink: " + min + " minutes and " + sec + " seconds ago", Toast.LENGTH_SHORT).show();
		}
		
		
		return false;
	}
	
	@Override
    public boolean onGenericMotionEvent(MotionEvent event) {
        gd.onTouchEvent(event);
        //Toast.makeText(this, "generic motion event", Toast.LENGTH_SHORT).show();
        return true;
    }

}
