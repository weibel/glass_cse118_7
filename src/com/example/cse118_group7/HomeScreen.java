package com.example.cse118_group7;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MotionEvent;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class HomeScreen extends Activity  implements GestureDetector.OnGestureListener {
 
    private GestureDetector gestureDetector;
    private RadioGroup mRGroup;
    private RadioButton maleButton;
    private RadioButton femaleButton;
    private TextView weightText;
    private String gender;
    private int currentWidget;
    private int weight;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_screen);
		gestureDetector = new GestureDetector(this, this);
		
		maleButton = (RadioButton) findViewById(R.id.male);
		femaleButton = (RadioButton) findViewById(R.id.female);
		weightText = (TextView) findViewById(R.id.weight);
		mRGroup = (RadioGroup) findViewById(R.id.radioGender);
		
		maleButton.requestFocus();
		currentWidget = 0;
		weight = 100;
		weightText.setText(""+weight);
		gender = "male";
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.home_screen, menu);
		return true;
	}
	
    @Override
    public boolean onGenericMotionEvent(MotionEvent event) {
        gestureDetector.onTouchEvent(event);
        //Toast.makeText(this, "generic motion event", Toast.LENGTH_SHORT).show();
        return true;
    }
    
	@Override
	public boolean onDown(MotionEvent arg0) {
		// TODO Auto-generated method stub
		//Toast.makeText(this, "on down", Toast.LENGTH_SHORT).show();
		return false;
	}

	@Override
	public boolean onFling(MotionEvent arg0, MotionEvent arg1, float x,
			float y) {


		weight += x/100;
		
		if (weight < 0) {
			weight = 0;
		}
		
		weightText.setText(""+weight);
		return false;
	}

	@Override
	public void onLongPress(MotionEvent arg0) {
		// TODO Auto-generated method stub
		//Toast.makeText(this, "on long press", Toast.LENGTH_SHORT).show();

		Intent i = new Intent(this, DrinkingActivity.class);
		i.putExtra("gender", gender);
		i.putExtra("weight", Integer.parseInt(weightText.getText().toString()));
		startActivity(i);
		

	}

	@Override
	public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2,
			float arg3) {
		// TODO Auto-generated method stub
		//Toast.makeText(this, "on scroll", Toast.LENGTH_SHORT).show();
		return false;
	}

	@Override
	public void onShowPress(MotionEvent arg0) {
		// TODO Auto-generated method stub
		//Toast.makeText(this, "on show presst", Toast.LENGTH_SHORT).show();
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent arg0) {
		// TODO Auto-generated method stub
		//Toast.makeText(this, "on single tap up", Toast.LENGTH_SHORT).show();

		if(femaleButton.isChecked()) {
			maleButton.toggle();
			gender = "male";
		} else if(maleButton.isChecked()) {
			femaleButton.toggle();
			gender = "female";
		}
		return false;
	}
}
